import os, urllib.request, urllib.parse, urllib.error, time, sys, requests
import numpy as np  # pip install numpy
import imghdr  # pip install imghdr
import cv2  # pip install opencv_python-3.2.0+contrib-cp36-cp36m-win_amd64.whl  
            #Or you can go with a more generic version that also works with older python versions:  pip install opencv-contrib-python
from PIL import Image  # pip install pillow
from selenium import webdriver  # pip install selenium -- also download the chromedriver binary for your OS from their website, put in the folder with this script,
                                # and change line 15 to match the filename of the binary.
import imagehash #Thank you Johannes Buchner!  https://github.com/JohannesBuchner/imagehash -- pip install imagehash
from pathlib import Path # I've done my best to make the paths insensitive to variations between Windows "\" and Linux or Mac "\".
                         # Problems may still exist - not tested yet on the latter two
#from io import BytesIO
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

#Download the chromedriver.exe (or linux/mac binary, depending on your OS) and place it in the same directory with this script
#and change the name below to match the binary you downloaded
def GetChromeDriver():
    chromedriverpath = str(Path(".")) + "/chromedriver.exe"  #"chromedriver.exe" is the Selenium chromedriver Windows binary.  Change to "chromedriver" for Linux binary.
    driver = webdriver.Chrome(str(chromedriverpath))
    return driver


def GetGoogleScrapeUrl(searchTerm):
    return "https://www.google.com/search?q=" + searchterm + "&safe=on&espv=2&biw=1599&bih=726&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiI56_7isXSAhXC4iYKHcbZCLEQAUIBigB#q=" + searchterm + "&safe=off&tbm=isch&tbs=sur:fc&*"


# This tests for image type (imghdr looks at first few bytes of image)
# If it has a type, it returns the image type extension.
# Otherwise, it returns an empty string.
def TestImageInMemory(image, imageLocalPath):
    imageCheck = imghdr.what("", image)

    if (imageCheck is not None):
        print("imghdr check - here's type: " + str(imageCheck))
        return imageLocalPath + "." + str(imageCheck)
    else:
        print("In memory test: image corrupt according to imghdr, not saving.")
        return ""


# imagePath:  Where to save the file on your local harddisk
# response:  The response from the request module with the image in the response.content.
def SaveImageToFile(response, imagePath):
    try:
        with open(imagePath, 'wb') as handle:
            for block in response.iter_content(1024):  #in order to be able to iterate over the image for write, we need the request module's response
                if not block:
                    break
                handle.write(block)
    except Exception as e:
        print("Exception writing file: " + str(e))
        return False
    print("Image saved to: " + imagePath)
    return True


# This function will download the image to memory
# and call the test method.  If it tests OK (that test is a bit limited)
# then it will save the file using the extension given by the test method.
def DownloadImage(image_url, imageLocalPath):
    print(image_url + " --> " + imageLocalPath)
    try:
        response = requests.get(image_url, stream=True)
        if not response.ok:
            print("Received bad response in download: " + response)
            return ""
        else:
            # We successfully downloaded the file, now lets test it & return the data if it's good:
            #Look at first few bytes of file for valid image type data.
            #If successful, imagePath will be a good path with extension.
            #Otherwise, it'll just be ""
            imagePath = TestImageInMemory(response.content, imageLocalPath)
            if imagePath != "":
                SaveImageToFile(response, imagePath)
            return imagePath
            
    #If the url was a bad link, catch the exception so we can continue with the
    #next image:
    except Exception as e:
        print("Exception downloading: " + str(e))
        return ""


def DeleteImage(imagePath, message):
    print(message + " " + "{}".format(imagePath))
    os.remove(imagePath)


#Checks the image by opening with opencv
#Returns true if corrupt and false if not
def ImageGoodForOpenCV(imagePath):
    imageCheck = cv2.imread(imagePath)
    if (type(imageCheck) is not np.ndarray):
        DeleteImage(imagePath, "[INFO] image corrupt, deleting:")
        return False
    print("OpenCV check OK.")
    return True


def GetListOfImagePathsInFolder(folderPath):
    return list(Path(folderPath).glob('*.*'))


def GetNumberOfFilesInFolder(folderPath):
    return len(GetListOfImagePathsInFolder(folderPath))

    
def GetImageFilePerceptualHash(imagePath):
    return imagehash.phash(Image.open(imagePath))


#May Josie's hash live forever somewhere happy.  To Josie.
def GeneratePerceptualHashList(folderPath):
    print("Generating hashlist for images .")
    imagePathList = GetListOfImagePathsInFolder(folderPath)
    imagePerceptualHashList = []
    for imagePath in imagePathList:
        imagePerceptualHashList.append(GetImageFilePerceptualHash(imagePath))
    return imagePerceptualHashList


def ImageHashIsDuplicate(imageHashList, imageHash):
    for hashFromList in imageHashList:
        if hashFromList == imageHash:
            #This will not be resistant to spamming - we aren't doing Earth Mover's Distance
            return True #the image matches the perceptual hash of another image in the save folder, so is not unique
    #The image is unique because the image did not match any of the imageHashes from the save folder
    return False


# Will make the folder if it does not exist.
# Returns True if the folder already existed and False if it had to be created.
def MakeFolderIfNotExists(folderPath):
    if not os.path.exists(folderPath):
        os.makedirs(folderPath)
        return False
    else:
        return True


def FolderCheck(folderPath):
    if MakeFolderIfNotExists(folderPath) == True:
        print("Image folder at " + folderPath + " already exists, getting all images perceptual hashes to avoid saving duplicates.")
        return GeneratePerceptualHashList(folderPath)
    else:
        return []


#Makes sure the image isn't a perceptual duplicate (somewhat resistant to noise and size changes)
#Adds the image hash to the imagePerceptualHashList if it is not a duplicate
def ImageIsNotDuplicate(imagePath, imagePerceptualHashList):
    imageHash = GetImageFilePerceptualHash(imagePath)
    if ImageHashIsDuplicate(imagePerceptualHashList, imageHash) == True:
        DeleteImage(imagePath, "[INFO] image is a duplicate, deleting:")
        return False #duplicate image
    else:
        imagePerceptualHashList.append(imageHash)
        print("Duplicate check OK.")
        return True


def FindUrl(anchorAttributeContainingImgUrl, startSubString, endSubString):
    if startSubString in anchorAttributeContainingImgUrl:
        image_url_raw = anchorAttributeContainingImgUrl.split(startSubString)[1].split(endSubString)[0]
        image_url = urllib.parse.unquote(urllib.parse.unquote(image_url_raw))
        #print(image_url)
        return image_url


def ValidateUrl(url, validator):
    try:
        validator(url)
    except (ValueError, ValidationError):
        return False
    return True

def GetAllUrls(browser, imageAnchorTagClassName, anchorAttributeContainingImgUrl, startSubString, endSubString):
    anchors = browser.find_elements_by_class_name(imageAnchorTagClassName)
    unparsedUrls = []
    urls = []
    validator = URLValidator(schemes=('http', 'https', 'ftp', 'ftps', 'rtsp', 'rtmp'))
    for anchor in anchors:
        unparsedUrls.append(anchor.get_attribute(anchorAttributeContainingImgUrl))
    for unparsedUrl in unparsedUrls:
        if ValidateUrl(url, validator):
            urls.append(FindUrl(unparsedUrl, startSubString, endSubString))
    return urls


def HandleScrolls(browser, number_of_scrolls, showMoreButtonText):
    #Handle scrolling and clicking the button to get more images
    for i in range(int(number_of_scrolls)):
        for j in range(100):
            browser.execute_script("window.scrollBy(0,10000)")
            time.sleep(.2)
        time.sleep(0.5)
        try:
            browser.find_element_by_xpath("//input[@value='" + showMoreButtonText + "']").click()  #Google: Show more results   Bing: See more images
        except Exception as e:
            print("Exception in HandleScrolls, going to try to find an anchor tag href to click: " + str(e))
            try:
                browser.find_element_by_xpath("//a[contains(.,'" + showMoreButtonText + "')]").click()
            except Exception as e:
                print("Exception in HandleScrolls, anchor tag did not work either: " + str(e))


#Call this method to have chromedriver open chrome, go to an image search engine, search for images, scroll, click show more, and download all images.
#It will check each image to make sure it is not corrupt, and it will check your download folder to make sure the image is not a duplicate of one you already have.
#Here's what to pass in as parameters:
#scrapeUrl - this is the full url for the image search engine, including any query parameters.
#number_of_scrolls - this is how many times you want it to scroll to the bottom and click the show more images button.
#showMoreButtonText - this is the exact text on the button that makes it show more images.
#imageAnchorTagClassName - if you do a manual search with a browser and open dev tools by pressing F12, you can right click and "inspect" one of the images displayed
#   - look for an anchor tag ('a' elements) that has the image url.  Note the class property value.  That's what you'll put in imageAnchorTagClassName.
#anchorAttributeContainingImgUrl - if you look at all the properties of the 'a' anchor tag, one of them will have the image url in it.
#   - put the name of that property in anchorAttributeContainingImgUrl
#startSubString - this is the text immediately before the image url in the 'a' anchor tag.  It should be enough text to make it unique.
#endSubString - this is the text immediately after the image url in the 'a' anchor tag.  Once again, it should be unique.
def ScrapeImages(browser,
    scrapeUrl, #Go to the image search engine, type a search term, and look at it to see what it becomes.
    number_of_scrolls, 
    showMoreButtonText, #Google: , Bing: 
    imageAnchorTagClassName, #Google:  'rg_l', Bing:  'iusc'
    anchorAttributeContainingImgUrl, #Google: "href", Bing: 
    startSubString, #Google:  "?imgurl=", Bing: "&quot;murl&quot;:&quot;"
    endSubString, #Google: , #Bing: 
    folderPath):   #The folder path will always be from the local directory:  ./images/<searchterm>/

    browser.get(scrapeUrl)
    HandleScrolls(browser, number_of_scrolls, showMoreButtonText)

    urls = GetAllUrls(browser, imageAnchorTagClassName, anchorAttributeContainingImgUrl, startSubString, endSubString)
    print("Found urls:")
    print(urls)
    
    imagePerceptualHashList = FolderCheck(folderPath) #if there are already images in the folder for this searchterm, don't overwrite them

    #Download all the images and only save them if they are not corrupt and not duplicates of what we already have:
    count = GetNumberOfFilesInFolder(folderPath) + 1
    for url in urls:
        if url != None and url != "":
            imagePath = folderPath + "/image" + str(count)  # The download method will set the file extension for the image.
            imagePath = DownloadImage(url, imagePath) #Now that we have the image url, let's download it
            if imagePath != "": #If DownloadImage was successful, imagePath will be full filepath + extension, otherwise it'll be "".
                #These functions test for image corruption first with OpenCV (deletes if corrupt)
                #Then checks whether the image is a duplicate with the imagehash module (deletes if it's a duplicate) 
                #       -- (must be last test - adds phash to list if the image is not a duplicate)
                if (ImageGoodForOpenCV(imagePath) and ImageIsNotDuplicate(imagePath, imagePerceptualHashList)):
                    count += 1

    print("Saved " + str(count) + " images from " + scrapeUrl + " to: " + folderPath)
    return count, imagePerceptualHashList


if __name__ == '__main__':
    searchterm = sys.argv[1] # get the search term from the first argument passed at the command line
    number_of_scrolls = sys.argv[2] #let the user pass in the number of scrolls to do

    browser = GetChromeDriver()
    
    url = GetGoogleScrapeUrl(searchterm)
    folderPath = str(Path("./images")) + "/" + searchterm

    print("Scraping Google Images for '" + searchterm + "' with " + number_of_scrolls + " scrolls.")
    url = "https://www.google.com/search?q=" + searchterm + "&safe=on&espv=2&biw=1599&bih=726&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiI56_7isXSAhXC4iYKHcbZCLEQAUIBigB#q=" + searchterm + "&safe=off&tbm=isch&tbs=sur:fc&*"
    totalImages, imagePerceptualHashList = ScrapeImages(browser,
        url, 
        number_of_scrolls, 
        "Show more results", 
        "rg_l", 
        "href", 
        "?imgurl=", 
        "&imgrefurl=", 
        folderPath)
    

    print("Scraping Bing Images for '" + searchterm + "' with " + number_of_scrolls + " scrolls.")
    url = "https://www.bing.com/images/search?q=" + searchterm + "&qs=n&form=QBILPG&sp=-1&pq=" + searchterm + "&sc=8-9&sk=&cvid=E176B637145C4EC4899DAF7492DC1CA3"
    totalImages, imagePerceptualHashList = ScrapeImages(browser,
            url, 
            number_of_scrolls, 
            "See more images", 
            "iusc", 
            "outerHTML", 
            "&quot;murl&quot;:&quot;", 
            "&quot;,&quot;", 
            folderPath)
    print("Finished scraping.  Downloaded " + str(totalImages - 1) + " total images.")

    print("Done.")
    browser.quit()