# webscraper

This repo has a Python webscraper that uses Selenium ChromeDriver.  It will acquire the image type and set the file extensions itself, check the image for corruption using OpenCV, and use imagehash perceptual hashing to check for duplicates.  It gets the images from Google and Bing image searches.

Download the Selenium binary for ChromeDriver from Selenium's website and place in this directory.  If you use a different operating system than Windows, you will have
to change the binary file name in the GetChromeDriver() function.

Usage:
"python scrape.py cat 2"
This will go to image search engines and search for cat, scroll down to the bottom and press the show more button (it will scroll down twice), then download all the images that have appeared (from their original sources).  Currently it goes to Google first, then to Bing.

Further information about the image check process:
While downloading, it uses imghdr to check the image data in memory for type (looks at first few bytes), and if valid downloads and saves to file with the proper extension.
After, it opens with OpenCV to check for image corruption, deleting if corrupt.
Then, it uses imagehash perceptual hashing to check for whether it's a duplicate, deleting if so.

In this way, we can get about 500 images per search without worrying about whether we're getting duplicates from earlier searches, and not experiencing any corrupt images.